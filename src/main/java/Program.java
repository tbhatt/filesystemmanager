import java.util.Scanner;

public class Program {
    public static String programName = "/FileManager/src/main/resources";
    public static Scanner sc;
    public static String currPath = System.getProperty("user.dir") + programName;
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        showFunctions();
        sc.close();
    }

    private static void showFunctions(){
        Boolean terminate = false;
        FileManipulation fManipulation = new FileManipulation();
        while(terminate == false){
            System.out.println("\nPlease choose one of the three functions");
            System.out.println("1: Show files in a chosen directory");
            System.out.println("2: Filter files by extension in a chosen directory");
            System.out.println("3: File manipulation");
            System.out.println("E: EXIT");

            System.out.print("Enter task: ");
            
            String task = sc.nextLine();
            if (task.equals("E") || task.equals("e")) {
                System.out.println("Terminating program...");
                terminate = true;
            }
            else if(task.equals("1")){
                System.out.println("Task 1 show files");
                fManipulation.showFiles();
            }
            
            else if(task.equals("2")){
                System.out.println("Task 2 filter files");
                fManipulation.filterFiles();
            }

            else if(task.equals("3")){
                System.out.println("Task 3 manipulate files");
                fManipulation.manipulateFile();
            }
        }
        fManipulation.closeScanner();
    }

}