import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;

public class Logger {
    long startTime = 0;
    File file;
    String path;
    String pathToLogFile = System.getProperty("user.dir") + "/../.."+"/FileManager/src/main/resources/logs/log.txt";
    public Logger(){
        
        path = pathToLogFile;
        try {
            file = new File(path);
            if(file.createNewFile()) System.out.println("File created: " + file.getName());
            else System.out.println("File already exists");

            PrintWriter writer = new PrintWriter(file);
            writer.print("");
            writer.close();

            
        } catch (IOException e) {
            System.out.println("Couldnt create logger file: " + e);
        }
    }   

    public void addLog(String task){
        long time = System.currentTimeMillis() - startTime;
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String summary = "\n" + timestamp + ":  " + task + " took " + time + " ms";
        System.out.println(summary);
        addToFile(summary);
    }

    public void startTimer(){
        startTime = System.currentTimeMillis();
    }

    public void addToFile(String summary){
        FileWriter fileWriter;
        BufferedWriter bufferedWriter;
        try{
            fileWriter = new FileWriter(file, true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(summary);
            bufferedWriter.close();

        } catch(IOException e){
            System.out.println("Error writing to logger file: " + e);
        }

    }

}
