import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;


public class FileManipulation{
    public static String programName = "/../src/main/resources";
    public static String currPath = System.getProperty("user.dir") + programName;
    public static Scanner sc;
    Logger logger;

    public FileManipulation(){
        sc = new Scanner(System.in);
        logger = new Logger();
    }

    public void closeScanner(){
        sc.close();
    }

    public void manipulateFile(){
        File file = chooseFile();
        //Name
        System.out.println("Name: " + file.getName());
        //Size
        long size = file.length();
        System.out.format("Size: %d bytes\n", size);
        //Lines
        int lines = 0;
        try {
            logger.startTimer();
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            while (reader.readLine() != null) lines++;
            System.out.println("Lines: " + lines);
            reader.close();
        } catch (Exception e) {
            System.out.println("Error opening file: " + e);
        }
        logger.addLog("Reading all lines");

        //Search for words
        Boolean search = true;
        String in;
        while(search){
            searchWord(file);
            System.out.println("Enter E to stop searching for words or anything to continue: ");
            in = sc.nextLine();
            if(in.equals("E") || in.equals("e")) search = false;
        }
    }
    private int searchWord(File file){
        int wordCount = 0;
        String line;
        try {
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            System.out.println("Enter word to search: ");
            String myWord = sc.nextLine();
            System.out.println("Searching for word: " + myWord);
            logger.startTimer();
            while((line = reader.readLine()) != null){
                String[] words = line.split(" ");
                for (String word : words) {
                    if(word.toLowerCase().equals(myWord.toLowerCase())){
                        wordCount++;
                    }
                }
            }
            System.out.println("Total times word was found: " + wordCount);
            logger.addLog("Searching for " + myWord);
            reader.close();
        } catch (Exception e) {
            System.out.println("Error searching word in file: " + e);
        }

        return wordCount;
    }
    private static File chooseFile(){
        System.out.println("First write path to where the file is");
        String p = getPath();
        //Testing if folder exists
        File folderpath = new File(p);
        Boolean folderExists = folderpath.exists();
        while(!folderExists){
            System.out.println("The path found no directory");
            p = getPath();
            folderpath = new File(p);
            folderExists = folderpath.exists();
        }
        //Show files in folder
        printDirectory(new File(p));
        //Testing and selecting a file in provided directory
        System.out.println("Select one file");
        String f = sc.nextLine();
        File file = new File(p+"/"+f);
        Boolean exists = file.exists();
        while (!exists) {
            System.out.println("Error: File not found");
            System.out.println("Choose a new file: ");
            f = sc.nextLine();
            file = new File(p+"/"+f);
            exists = file.exists();            
        }
        return file;
    }

    public File showFiles(){
        try {
            String p = getPath();
            System.out.println("Path is " + p);
            File path = new File(p);
            printDirectory(path);
            return path;

        } catch (Exception e) { 
            System.out.println("Error showing files in directory. Find the correct path. Exiting task" + e);
        }
        return null;
    }

    private static void printDirectory(File dir){
        String content[] = dir.list();
        System.out.println("List of files and directories in current directory:");
        for(int i=0; i<content.length; i++) {
            System.out.println(content[i]);
        }
    }

    public void filterFiles(){
        try{
            File dir = showFiles();
            String[] files = dir.list();
            System.out.println("Specify extension for files you want: ");
            String ext = sc.nextLine();
            while(ext.length() < 2){
                System.out.print("Invalid extension, try again: ");
                ext = sc.nextLine();
            }

            ArrayList<String> filteredFiles = new ArrayList<String>();
            for (String file : files) {
                if(getFileExtension(file).equals(ext)){
                    filteredFiles.add(file);
                }   
            }
            
            System.out.println("All " + ext + "files: " );
            for (String file : filteredFiles) {
                System.out.print("* " + file);
            }
        }
        catch(Exception e){
            System.out.println("Error filtering files by extension" + e);
        }
    }

    private static String getPath(){
        printDirectory(new File(currPath));
        System.out.print("Path for directory?: ");
        String path = sc.nextLine();
        while(path.length() < 2){
            System.out.print("Invalid Path");
            path = sc.nextLine();
        }
        return currPath + path;
    }

    private static String getFileExtension(String name) {
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }
    
}
