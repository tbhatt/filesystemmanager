# Java File Manager
This is a project where you can:
1. Show files in a chosen directory
2. Filter files in a directory by their extension
3. Manipulate file (Read lines and search for words)

## How to compile the program
#### javac -d out src/main/java/*.java
![Image](src/main/resources/images/FileManagerCompile.PNG)

## How to create jar file 
#### jar cfe FileManager.jar Program *.class
![Image](src/main/resources/images/FileManagerJar.PNG)

## How to run jar file
#### java -jar FileManager.jar
![Image](src/main/resources/images/FileManagerRunningJar.PNG)